package com.itcat.itcat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class Frag2Activity extends AppCompatActivity
        implements BlankFragment.BlankFragmentInteraction {

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "ss";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag2);
        createNotificationChannel();
//        try {
//            Thread.sleep(6000);
//        } catch (InterruptedException e) {
//            Log.d("Frag2Activity", "Error : " + e.getMessage());
//        }
        Intent intent = new Intent(this, MyIntentService.class);
        intent.putExtra("text", "Hi");
        startForegroundService(intent);

        Intent intent2 = new Intent(this, MyIntentService.class);
        intent2.putExtra("text", "bye");
        startForegroundService(intent2);


        SharedPreferences sp = getSharedPreferences("default"
                , MODE_PRIVATE);

        boolean firstTime = sp.getBoolean("first_time", true);
        if (firstTime) {
            Toast.makeText(this, "First time", Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("first_time", false);

            editor.apply();
        }

        if (savedInstanceState == null) {


            Bundle bundle = new Bundle();
            bundle.putString("t", "Something");
            BlankFragment blankFragment = new BlankFragment();
            blankFragment.setArguments(bundle);
            blankFragment.doSth();


            BlankFragment2 blankFragment2 = new BlankFragment2();

            FragmentManager fragmentManager =
                    getSupportFragmentManager();

            fragmentManager.beginTransaction()
                    .add(R.id.top, blankFragment)
                    .addToBackStack("blankFragment")
                    .commit();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction = fragmentTransaction.add(R.id.bottom, blankFragment2);
            fragmentTransaction = fragmentTransaction.addToBackStack("blankFragment2");
            fragmentTransaction.commit();


//            fragmentManager.beginTransaction()
//                    .remove(blankFragment)
//                    .commit();
//
//            fragmentManager.beginTransaction()
//                    .add(R.id.top,blankFragment2)
//                    .commit();
            BlankFragment2 b2 = new BlankFragment2();
            fragmentManager.beginTransaction()
                    .replace(R.id.top, b2)
                    .addToBackStack("b2")
                    .commit();

            b2.doSth();

        }

    }

    @Override
    public void onClicked(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    OnBatteryLowBroadcastReciver onBatteryLowBroadcastReciver;

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_LOW);
        onBatteryLowBroadcastReciver = new
                OnBatteryLowBroadcastReciver();
        registerReceiver(onBatteryLowBroadcastReciver, ifilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(onBatteryLowBroadcastReciver);
    }
}
