package com.itcat.itcat;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {


    interface BlankFragmentInteraction {
        void onClicked(String s);
    }

    BlankFragmentInteraction callback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (BlankFragmentInteraction) context;
    }

    public BlankFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    TextView textView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        callback.onClicked("fds");

        textView = view.findViewById(R.id.tv);

        Bundle bundle = getArguments();
        if (bundle != null) {
            textView.setText(
                    bundle.getString("t", "Default")
            );
        }

        Toast.makeText(getContext(), "fds", Toast.LENGTH_LONG).show();


        String string = getContext()
                .getResources()
                .getString(R.string.app_name);

    }

    void doSth() {

    }

}
