package com.itcat.itcat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);


        CreateOrder createOrder = new CreateOrder();
        createOrder.setCost("132");

//        final Gson gson = new GsonBuilder().create();
//
//        String jsonBody = gson.toJson(createOrder);
//        Log.d("api", jsonBody);
//
//        OkHttpClient client = new OkHttpClient();
//
//        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType, jsonBody);
//        Request request = new Request.Builder()
//                .url("http://104.248.161.120:8080/tallb-1.0.0/api/v1/students/orders/2")
//                .put(body)
//                .addHeader("Accept", "application/json")
//                .addHeader("Content-Type", "application/json")
//                .addHeader("cache-control", "no-cache")
//                .build();
//
//        client.newCall(request)
//                .enqueue(new Callback() {
//                    @Override
//                    public void onFailure(Call call, IOException e) {
//                        Log.e("api", e.getMessage());
//
//                    }
//
//                    @Override
//                    public void onResponse(Call call, Response response) throws IOException {
//                        String bodyResponse = response.body().string();
//                        CreateOrderResponse createOrderResponse = gson.fromJson(bodyResponse, CreateOrderResponse.class);
//                        Log.d("api", "Response : " + createOrderResponse.getId());
//
//                    }
//                });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://104.248.161.120:8080/tallb-1.0.0/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        StudentOrdersApi api = retrofit.create(StudentOrdersApi.class);

        api.getOrders()
                .enqueue(new retrofit2.Callback<List<StudentOrder>>() {
                    @Override
                    public void onResponse(retrofit2.Call<List<StudentOrder>> call, retrofit2.Response<List<StudentOrder>> response) {
                        Log.d("api11", response.toString());
                    }

                    @Override
                    public void onFailure(retrofit2.Call<List<StudentOrder>> call, Throwable t) {
                        Log.e("api11", t.getMessage());

                    }
                });
    }
}
