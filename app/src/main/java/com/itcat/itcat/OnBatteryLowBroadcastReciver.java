package com.itcat.itcat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class OnBatteryLowBroadcastReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("battery","low");
        Toast.makeText(context,"Battery low",Toast.LENGTH_LONG).show();
    }
}
