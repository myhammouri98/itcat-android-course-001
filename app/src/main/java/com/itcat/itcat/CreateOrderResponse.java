package com.itcat.itcat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * Created by Mohammed Alhammouri on 07,January,2019
 * myhammouri98@gmail.com
 */
public class CreateOrderResponse {

    @SerializedName("id")
    @Expose
    long id;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
