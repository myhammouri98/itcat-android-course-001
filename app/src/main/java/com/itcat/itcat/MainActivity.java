package com.itcat.itcat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;


public class MainActivity extends AppCompatActivity
//        implements View.OnClickListener {
{

    public static final String TAG = "MainActivity";

    //    @BindView(R.id.button)
    Button button;
    //
//    @BindView(R.id.tv1)
    TextView textView;

    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "On create");
        setContentView(R.layout.activity_main);

//        ButterKnife.bind(this);


        iv = findViewById(R.id.imageView);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.tv1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent nextActivity = new Intent(MainActivity.this,ActivityB.class);
//                startActivity(nextActivity);

//                Intent sendIntent = new Intent();
//                sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi");
//                sendIntent.setType("text/plain");

// Verify that the intent will resolve to an activity
//                if (sendIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivity(sendIntent);
//                }else{
//                    Toast.makeText(MainActivity.this,"No activity can handle this crieteria", Toast.LENGTH_LONG).show();
//                }

                Intent intent = new Intent(MainActivity.this,ActivityB.class);
                startActivity(intent);

//                capturePhoto("test.jpg");
            }
        });

        textView.setText("fd");
        if (savedInstanceState != null) {

            textView.setText(
                    savedInstanceState.getString("tv1")
            );

        }
//        textView.setText("It cat");

//        View.OnClickListener onClick1 =
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        //dfsfasdfadf
//                        textView.setText("on click1");
//
//                    }
//                };
//
//        View.OnClickListener onClick2 =
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        //dfsfasdfadf
//                        textView.setText("on click2");
//
//                    }
//                };
//
//
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                textView.setText("fdsfsdf");
//            }
//        });
//
//
//        button.setOnClickListener(this);

    }


    static final int REQUEST_IMAGE_CAPTURE = 1;

    public void capturePhoto(String targetFilename) {

        File file = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        Uri uri = Uri.fromFile(file);

        final Uri mLocationForPhotos =FileProvider.getUriForFile(
                MainActivity.this,
                "com.itcat.itcat.provider", //(use your app signature + ".provider" )
                getExternalFilesDir(Environment.DIRECTORY_PICTURES));

        Log.d(TAG,"Path : " + getExternalFilesDir(Environment.DIRECTORY_PICTURES));
        Log.d(TAG,"URI : " + mLocationForPhotos.getPath());
        String s = "fd";
        s += "ss";


        Uri photoPath = Uri.withAppendedPath(mLocationForPhotos, "t.jpg");

        Log.d(TAG,"image URI : " + photoPath.getPath());

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
               photoPath);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bitmap thumbnail = data.getParcelableExtra("data");
//            Log.d(TAG,"bitmap : " + thumbnail.toString());
            iv.setImageBitmap(thumbnail);
            // Do other work with full size photo saved in mLocationForPhotos
        }
    }

//
//    @Override
//    public void onClick(View v) {
//        int id = v.getId();
//
//        switch (id) {
//            case R.id.button:
//                textView.setText("fdsfsd");
//                break;
//            case R.id.button2:
//                textView.setText("awqwwq");
//                break;
//
//
//        }
//
//    }

//    public void b1Clicked(View v) {
//
//    }

//    @OnClick(R.id.button)
//    public void onB1Clicked(View v){
//        textView.setText("sssss");
//    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tv1", textView.getText().toString());
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        textView.setText(
                savedInstanceState.getString("tv1")
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "On start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "On Resume");


//        new AlertDialog.Builder(this)
//                .setTitle("hi")
//                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "On pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "On stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "On destroy");
    }


}
