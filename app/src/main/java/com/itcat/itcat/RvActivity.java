package com.itcat.itcat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RvActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    MyRvAdapter adapter;
    List<String> list =
            new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rv);

        recyclerView = findViewById(R.id.rv);

        LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(this);

        GridLayoutManager gridLayoutManager =
                new GridLayoutManager(this,2);

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        list.add("gdfdssdfsdf");
        list.add("gdfdssdfsasdfsaddf");
        list.add("gdfdssdfsdzxzdsfxczxccf");
        list.add("gdfdssdfsqreweqweqdf");
        list.add("gdfdssdfsd21312f");
        list.add("gdfdssdfsvcxzvxcdf");
        list.add("gdfdssdfsdffsdafdsfs");
        list.add("gdfdssdfsdkl,;lkf");
        list.add("gdfdssdfsdffasdfasd");
        list.add("gdfdssdfsdfafsdfasd");
        list.add("gdfdssdfsfasdfdasfdf");

        adapter = new MyRvAdapter(list);
        recyclerView.setAdapter(adapter);

    }
}
