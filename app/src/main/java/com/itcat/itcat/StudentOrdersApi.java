package com.itcat.itcat;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/*
 * Created by Mohammed Alhammouri on 07,January,2019
 * myhammouri98@gmail.com
 */
public interface StudentOrdersApi {

    @GET("students/orders")
    Call<List<StudentOrder>> getOrders();

}
