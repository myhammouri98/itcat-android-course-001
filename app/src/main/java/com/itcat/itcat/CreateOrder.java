package com.itcat.itcat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateOrder {

    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("expertyId")
    @Expose
    private Integer expertyId;
    @SerializedName("finishAt")
    @Expose
    private String finishAt;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userName")
    @Expose
    private String userName;

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getExpertyId() {
        return expertyId;
    }

    public void setExpertyId(Integer expertyId) {
        this.expertyId = expertyId;
    }

    public String getFinishAt() {
        return finishAt;
    }

    public void setFinishAt(String finishAt) {
        this.finishAt = finishAt;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}