package com.itcat.itcat;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyIntentService");
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onHandleIntent(Intent intent) {
        Intent notificationIntent = new Intent(this, Frag2Activity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification =
                new Notification.Builder(this, "1")
                        .setContentTitle("hi")
                        .setContentText("My foreground notification")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentIntent(pendingIntent)
                        .setTicker("Ticker")
                        .build();


        startForeground(22, notification);


        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            Log.d("MyIntentService", "Error : " + e.getMessage());
        }


        if (intent != null) {
            final String text = intent.getStringExtra("text");
            Log.d("MyIntentService", text);
        }
    }

}
