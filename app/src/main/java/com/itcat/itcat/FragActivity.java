package com.itcat.itcat;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FragActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frag);

        FragmentManager fragmentManager =
                getSupportFragmentManager();


        BlankFragment blankFragment = new BlankFragment();
        fragmentManager.beginTransaction()
                .add(R.id.frag_container, blankFragment)
                .commit();


    }

}
