package com.itcat.itcat;

/*
 * Created by Mohammed Alhammouri on 05,January,2019
 * myhammouri98@gmail.com
 */
public class User {

    enum Gender{
        MALE,FEMALE
    }

    private String name;
    private Gender gender;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
