package com.itcat.itcat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class AsyncTaskActivity extends AppCompatActivity {

    private TextView mUserNameTv;
    private ProgressBar mLoadingUserPb;
    private GetUserAsyncTask getUserAsyncTask;

    boolean canRun = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        createNotificationChannel();

        mUserNameTv = findViewById(R.id.username_tv);
        mLoadingUserPb = findViewById(R.id.loading_user_pb);

        getUserAsyncTask = new GetUserAsyncTask("users.db");

        getUserAsyncTask.execute(1L);

    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Default channel";
            String description = "Some description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("default", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }


    class GetUserAsyncTask extends AsyncTask<Long, Integer, User> {

        private String database;

        GetUserAsyncTask(String database) {
            this.database = database;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mUserNameTv.setText("Fetching user...");
            mLoadingUserPb.setVisibility(View.VISIBLE);
        }

        @Override
        protected User doInBackground(Long... longs) {
            int progress = 0;
            try {
                Thread.sleep(1000);
                progress++;
                publishProgress(progress);
            } catch (InterruptedException e) {
                return null;
            }
            try {
                Thread.sleep(1000);
                progress++;
                publishProgress(progress);
            } catch (InterruptedException e) {
                return null;
            }
            try {
                Thread.sleep(1000);
                progress++;
                publishProgress(progress);
            } catch (InterruptedException e) {
                return null;
            }
            try {
                Thread.sleep(1000);
                progress++;
                publishProgress(progress);
            } catch (InterruptedException e) {
                return null;
            }

            User user = new User();
            user.setName("Mohammed");
            user.setGender(User.Gender.MALE);
            return user;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mLoadingUserPb.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            if (user != null) {
                if (mUserNameTv != null) {
                    mUserNameTv.setText(user.getName());
                }
            }
            if (mLoadingUserPb != null) {
                mLoadingUserPb.setVisibility(View.GONE);
            }
            Intent intent = new Intent(AsyncTaskActivity.this, AsyncTaskActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(AsyncTaskActivity.this, 0, intent, 0);


            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(AsyncTaskActivity.this, "default")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("New user")
                    .setContentText(user.getName())
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(user.getName() + "\n" + user.getGender().toString()))
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(AsyncTaskActivity.this);
            notificationManager.notify(1, mBuilder.build());


            if (canRun) {
                new GetUserAsyncTask("users.db")
                        .execute(2L);
                canRun = false;
            }

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (getUserAsyncTask != null) {
            getUserAsyncTask.cancel(true);
        }
    }


}
